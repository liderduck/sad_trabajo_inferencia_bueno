# README #

Parte del trabajo final de SAD(sistemas de apoyo a la decisión). 

### ¿Que hace este repositorio? ###

* Este programa lee un fichero .arff.
* Realiza un k-fold crossvalidation esto es parte el archivo en K partes y coge una para testear y el resto para entrenar.
* Procesa el entrenador con diferentes algoritmos y filtros.
* Va rotando el archivo de testeo y los de entreno para obtener diferentes datos.
* Imprime por pantalla el porcentaje de aciertos.

### ¿Como funciona este repositorio? ###

* Este programa lee un fichero .arff mediante path introducido por el usuario.
* Crea un clasificador de redes bayesianas (bayes net).
* Crea el evaluador.
* Evalúa el fichero mediante k-foldcrossValidation.
* Imprime por pantalla los resultados del evaluador.

### Tecnología ###

* Java
* API de Weka(http://www.cs.waikato.ac.nz/ml/weka/)

### Creador ###

* Jonathan Guijarro Garcia