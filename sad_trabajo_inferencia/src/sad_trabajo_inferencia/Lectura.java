package sad_trabajo_inferencia;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.Randomize;

public class Lectura {

	public Instances cargarDatos(String path) throws Exception{
	
		// 1.2. Open the file
		FileReader fi=null;
	/*	String path = null;
		
		System.out.println("Introduce el path del fichero .arff con su extension");
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));   
		path = bfr.readLine(); 
	*/	
		try {
			fi= new FileReader(path); //(args[0]) <-> ("~/software/weka-3-6-9/data/breast-cancer.arff" )
		} catch (FileNotFoundException e) {
				System.out.println("ERROR: Revisar path del fichero de datos:"/*+ruta*/);
		}
		// 1.3. Load the instances
		Instances data=null;
		try {
			data = new Instances(fi);
		} catch (IOException e) {
			System.out.println("ERROR: Revisar contenido del fichero de datos: "/*+ruta*/);
		}
		
		// 1.4. Close the file
		try {
			fi.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		
		// 1.6. EL for encuentra la clase, siempre y cuando se llame class
			//	data.attribute("class");
		int pos =0;
		for(int t=0;t<data.numAttributes();t++){
			if (data.attribute(t).name().equalsIgnoreCase("class")){
				pos=t;
			}
		}
		Randomize filtroRand = new Randomize();//creamos el filtro
		filtroRand.setInputFormat(data);//le asignamos los datos a filtrar
		Instances datosRan = Filter.useFilter(data, filtroRand);//creamos las nuevas instances usando el filtro.<
		
		datosRan.setClassIndex(0);	
		return datosRan;
	}

}
