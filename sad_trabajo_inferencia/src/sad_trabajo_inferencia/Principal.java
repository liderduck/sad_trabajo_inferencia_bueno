package sad_trabajo_inferencia;




import weka.core.Instances;

import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.bayes.net.estimate.SimpleEstimator;
import weka.classifiers.bayes.net.search.local.TAN;


	public class Principal {
		
	    public static void main(String[] args) throws Exception {
	    	Lectura lect= new Lectura();	    	
			Results resultados = new Results();
			
	    	/////////////Lectura de datos y aplica el filtro RANDOMIZE/////////////
	    	Instances dataSet;
	    	dataSet = lect.cargarDatos(args[0]);
	    	
	    	
	   /* 	
	    	System.out.println("creamos el naiveBayes para el baseline");
	    	//creamos el clasificador para baseline, hemos cogido el naivebayes
	    	NaiveBayes naivebayes = new NaiveBayes();
	    */	
	    	//creamos la particion para el holdout
	    	int trainSize = (int) Math.round(dataSet.numInstances() * 0.7);
			int testSize = dataSet.numInstances() - trainSize;
			
			Instances train = new Instances(dataSet, 0, trainSize);
			Instances test = new Instances(dataSet, trainSize, testSize);
		/*	
	    	//cogemos los archivos para crear el holdout
	    	naivebayes.buildClassifier(train);
	    	
	    	//creamos un evaluador
	    	Evaluation evaluador = new Evaluation(test);
	    	evaluador.evaluateModel(naivebayes, test);
	    	
	    	System.out.println("metodo hold out");
	    	resultados.imprimirResultados(evaluador);
	    	System.out.println("");
	    	
	    	//sistema no honesto
	    	Evaluation evaluadorNoHon = new Evaluation(dataSet);
	    	naivebayes.buildClassifier(dataSet);
	    	evaluadorNoHon.evaluateModel(naivebayes, dataSet);
	    	
	    	System.out.println("metodo no honesto");
	    	resultados.imprimirResultados(evaluadorNoHon);
	    	System.out.println("");
	    	
	    	//kfold cross validation.
	    	Evaluation evaluadorKFold = new Evaluation(dataSet);
	    	evaluadorKFold.crossValidateModel(naivebayes, dataSet, 10,new Random(1));
	    	System.out.println("metodo  kfold");
	    	resultados.imprimirResultados(evaluadorKFold);
	    	System.out.println("");
	    */	
	    	//creamos el clasificador modelo el bayesNet con la configuracion sacada del apartado anterior
	    	BayesNet bayesnet = new BayesNet();
	    	TAN tan = new TAN();
	    	SimpleEstimator simple = new SimpleEstimator();
	    	
	    	bayesnet.setEstimator(simple);
	    	bayesnet.setSearchAlgorithm(tan);
	    	
	    	//cogemos los archivos para crear el holdout
	    	bayesnet.buildClassifier(train);
	    /*	
	    	//creamos un evaluador
	    	Evaluation evaluadorNetHold = new Evaluation(test);
	    	evaluadorNetHold.evaluateModel(bayesnet, test);
	    	
	    	System.out.println("metodo hold out");
	    	resultados.imprimirResultados(evaluadorNetHold);
	    	System.out.println("");
	    	
	    	//sistema no honesto
	    	Evaluation evaluadorNetNoHon = new Evaluation(dataSet);
	    	bayesnet.buildClassifier(dataSet);
	    	evaluadorNetNoHon.evaluateModel(bayesnet, dataSet);
	    	
	    	System.out.println("metodo no honesto");
	    	resultados.imprimirResultados(evaluadorNetNoHon);
	    	System.out.println("");
	    */	
	    	//kfold cross validation.
	    	Evaluation evaluadorNetKFold = new Evaluation(dataSet);
	    	evaluadorNetKFold.crossValidateModel(bayesnet, dataSet, 10,new Random(1));
	    	System.out.println("metodo  kfold");
	    	resultados.imprimirResultados(evaluadorNetKFold);
	    	System.out.println("");
	    	
	    	
	    	
	    	
	    }
}


